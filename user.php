<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/class/autoloader.php");

session_start();
$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";

if ($method === "GET" && !empty($_SESSION["username"])) {
    $username = !empty($_GET["username"])? $_GET["username"]: $_SESSION["username"];
    
    $user = new User();
    if ($user->get_user($username) === false) {
        Response::send(null, 400, "User {$username} does not exist");
    }
    
    Response::send($user);

} else if ($method === "POST" && !empty($_SESSION["role"]) && $_SESSION["role"] === "admin") {

    $request = Request::parse();

    $errors = [];
    if (empty($request->username)) {
        array_push($errors, "username is empty");
    }
    if (empty($request->password)) {
        array_push($errors, "password is empty");
    } else if (strlen($request->password) < 6 || strlen($request->password) > 48) {
        array_push($errors, "password should be between 6 and 48 characters");
    }
    if (empty($request->role)) {
        array_push($errors, "role is empty");
    }

    if (!empty($errors)) {
        Response::send(null, 400, implode("\n", $errors));
    }

    $user = new User();
    $user->username = $request->username;
    $user->role = $request->role;
    $user->set_password($request->password);

    if ($user->register() === false) {
        Response::send(null, 400, "Invalid role");
    }

    Response::send($user, 201, "New user created");

} else if ($method === "DELETE" && !empty($_SESSION["username"]) && ((!empty($_GET["username"]) && ($_SESSION["username"] === $_GET["username"] || $_SESSION["role"] === "admin")) || empty($_GET["username"]))) {

    $delete_self = (!empty($_GET["username"]) && $_SESSION["username"] === $_GET["username"]) || empty($_GET["username"]);

    $user = new User();
    $user->username = $delete_self? $_SESSION["username"]: $_GET["username"];

    if ($user->username === "root") {
        Response::send(null, 400, "Cannot delete the root user");
    }
    if ($user->get_user($user->username) === false) {
        Response::send(null, 404, "User {$user->username} does not exist");
    }

    if ($user->delete() === true) {
        Response::send(null, 200, "Deleted user {$user->username}");
        if ($delete_self) {
            $_SESSION = [];
            session_destroy();
        }
    }
    Response::send(null, 500, "Error in deleting user");

} else {
    Response::not_found();
}