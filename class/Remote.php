<?php
class Remote {
    static function get($url, $headers = []) {
        $ch = self::req($url, "GET", $headers);
        return self::ret($ch);
    }

    static function patch($url, $data = [], $headers = []) {
        $ch = self::req($url, "PATCH", $headers);
        try {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_SLASHES)); 
        } catch (Exception $e) {
            return false;
        }
        return self::ret($ch);
    }
    
    static function ret($ch) {
        $url_response = curl_exec($ch);
        if (curl_errno($ch)) {
            return json_decode(json_encode(["message" => "Client cannot be reached"]));
        }

        $result = Request::parse($url_response);
        if (is_null($result)) {
            return json_decode(json_encode(["message" => "Something wrong with client"]));
        }

        if (!empty($result->data)) {
            $result = $result->data;
        } else {
            return json_decode(json_encode(["message" => $result->message]));
        }

        curl_close($ch);
        return $result;
    }
    
    static function req($url, $method, $headers) {
        $ch = curl_init();
    
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2); 
    
        return $ch;
    }
}