<?php
class System {
    private $available_visibility = ["all", "restricted"];
    
    function create() {
        
        $name = $this->name;
        $system_key = $this->system_key;
        $endpoint = $this->endpoint;
        $visibility = $this->visibility;
        if (array_search($visibility, $this->available_visibility) === false) {
            return false;
        }

        Database::init();
        Database::query("INSERT INTO Systems
                (name, system_key, endpoint, visibility, created) 
            VALUES 
                (:name, :system_key, :endpoint, :visibility, :created)",
            [ ":name" => $name, ":system_key" => $system_key, ":endpoint" => $endpoint, ":visibility" => $visibility, ":created" => time()]);

        $this->id = Database::insert_id();

        return true;
    }
    
    function get($id, $get_from_all = false, $partial = false) {
        Database::init();

        if ($get_from_all) {
            $sys = Database::query("SELECT id, name, system_key, endpoint, last_update, visibility, created FROM Systems WHERE
                id = :id"
                , 
                [
                    ":id" => $id
                ]
            );
        } else {
            $sys = Database::query("SELECT id, name, system_key, endpoint, last_update, visibility, created FROM Systems WHERE
                id = :id AND visibility = :visibility", 
                [
                    ":id" => $id,
                    ":visibility" => "all"
                ]
            );
        }
        
        if (count($sys) < 1) {
            return false;
        }
        
        $this->assign_values($sys[0]);

        if (!$partial) {
            $this->info = $this->update_values();
            if (empty($this->info->message)) {
                $this->update_last_seen();
            }
        }
        return $sys[0];
    }

    function update() {
        if (array_search($this->visibility, $this->available_visibility) === false) {
            return false;
        }
        Database::init();
        return Database::query(
            "UPDATE Systems
                SET 
                    name = :name,
                    system_key = :system_key,
                    endpoint = :endpoint,
                    visibility = :visibility
                WHERE
                    id = :id
                LIMIT 1",
            [
                ":id" => $this->id,
                ":name" => $this->name,
                ":system_key" => $this->system_key,
                ":endpoint" => $this->endpoint,
                ":visibility" => $this->visibility,
                ]
                
        );
    }
    
    function update_last_seen() {
        Database::init();
        return Database::query(
            "UPDATE Systems
                SET
                    last_update = :last_update
                WHERE
                    id = :id
                LIMIT 1",
            [
                ":id" => $this->id,
                ":last_update" => time(),
            ]
                
        );
    }

    
    function delete() {
        Database::init();
        return Database::query("DELETE FROM Systems WHERE id = :id", [":id" => $this->id]);
    }
    
    function assign_values($query_result) {
        foreach ($query_result as $key => $value) {
            $this->$key = $value;
        }
    }
    
    function shutdown() {
        $result = Remote::get("{$this->endpoint}/shutdown.php", ["Authorization: Bearer {$this->system_key}"]);
        return $result->message;
    }

    function update_values() {
        $result = Remote::get("{$this->endpoint}/info.php", ["Authorization: Bearer {$this->system_key}"]);
        return $result;
    }
    
    function get_overview() {
        $this->info = Remote::get("{$this->endpoint}/overview.php", ["Authorization: Bearer {$this->system_key}"]);
    }
}