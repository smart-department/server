<?php
class User {
    private $error = "";
    private $password;
    private $available_roles = ["admin", "user"];
    
    function register() {
        
        $username = $this->username;
        $password = $this->password;
        $role = $this->role;
        if (array_search($role, $this->available_roles) === false) {
            return false;
        }

        $password_hash = password_hash($password, PASSWORD_DEFAULT);
        
        Database::init();
        Database::query("INSERT INTO Users 
                (username, password, role, created) 
            VALUES 
                (:username, :password, :role, :created)",
            [ ":username" => $username, ":password" => $password_hash, ":role" => $role, ":created" => time()]);
        $this->id = Database::insert_id();

        return true;
    }

    function login($username, $password) {
        $usr = $this->get_user($username);

        if ($usr === false || !password_verify($password, $usr["password"])) {
            return false;
        }
    }

    function delete() {
        Database::init();
        return Database::query(
            "DELETE FROM Users WHERE username = :username LIMIT 1",
            [
                ":username" => $this->username
            ]
        );
    }
    
    function get_user($username) {
        Database::init();
        $usr = Database::query("SELECT id, username, role, password, created FROM Users WHERE
            username = :username", 
        [
            ":username" => $username
        ]);
        
        if (count($usr) < 1) {
            return false;
        }
        
        $this->assign_values($usr[0]);
        return $usr[0];
    }
    
    function get_id($id) {
        Database::init();
        $usr = Database::query("SELECT 
            id, 
            username, 
            role,
            password,
            created
        FROM Users WHERE 
            id = :id", 
        [
            ":id" => $id
        ]);
        
        if (count($usr) < 1) {
            return false;
        }
        
        $this->assign_values($usr[0]);
        return $usr[0];
    }

    function assign_values($query_result) {
        foreach ($query_result as $key => $value) {
            $this->$key = $value;
        }
    }

    function get_error() {
        return $this->error;
    }

    function set_password($password) {
        $this->password = $password;
    }
}