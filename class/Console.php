<?php
  class Console {
    public static $num = [
      "error" => 0,
      "success" => 0,
      "task" => 0
    ];

    static function is_cli() {
      return (php_sapi_name() === "cli");
    }

    static function print($text = "") {
      echo "{$text}\n";
    }

    static function success($text = "") {
      self::$num["success"]++;
      echo "\033[1;37m\033[1;42m SUCCESS \033[0m  {$text}\n";
    }
    
    static function task($text = "") {
      self::$num["task"]++;
      self::warn($text, " TASK - ". self::$num["task"] ." ");
    }
    
    static function warn($text = "", $prompt = "WARN") {
      echo "\033[0;30m\033[1;43m {$prompt} \033[0m  {$text}\n";
    }
    
    static function error($text = "") {
      self::$num["error"]++;
      echo "\033[1;37m\033[1;41m ERROR \033[0m  {$text}\n";
    }

    static function end() {
      echo "STATUS: success: ". self::$num["success"] .", errors: ". self::$num["error"] ."\n";
      exit(-1);
    }

    static function fatal($text = "") {
      self::error($text);
      self::end();
    }

    static function get_input($prompt, $default = "") {
      $val = readline($prompt);
      return ($val == "")? $default: $val;
    }
  }
