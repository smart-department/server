<?php
class Request {
    static function parse($raw = "") {
        $raw_value = empty($raw)? file_get_contents("php://input"): $raw;
        try {
            $data = json_decode($raw_value, false, 512, JSON_THROW_ON_ERROR);
            return $data;
        } catch (JsonException $e) {
            if (empty($raw)) {
                Response::send(null, 400, "Ill formated data given");
            }
        }
    }
}