<?php
require_once (__DIR__ . "/class/autoloader.php");

$KEYS = new Keys();

if (!Console::is_cli()) {
    Console::print ("only use in command line");
    Console::end();
}

$available_modes = ["config", "init", "tables-create", "tables-delete"];
$usage = "Usage: \nphp {$argv[0]} [root_key] mode\n\nROOT_KEY - should be defined in configuration (run php {$argv[0]} config)\n\nMODES - " . implode(", ", $available_modes) . "\n";

if ($argc === 2 && $argv[1] === "config") {
    // init config
    Console::print ("This script will create a .env file in your current directory\n");
    if (file_exists(".env")) {
        Console::warn("A configuration file already exists.");
        if (Console::get_input("Are you sure to overwrite it? (y or n) (default y):", "y") === "n") Console::end();
    }

    $req_values = [
        "ROOT_KEY" => ["This key is required to do certain operations in init.php", "1234"],
        "ROOT_PASSWORD" => ["The password for the root account", "password"],
        "DATABASE_HOST" => ["MySQL database hostname", "localhost"],
        "DATABASE_NAME" => ["MySQL database name", "smart_department_server"],
        "DATABASE_USERNAME" => ["MySQL username to login to database", "root"],
        "DATABASE_PASSWORD" => ["MySQL login password", "password"]
    ];

    foreach ($req_values as $key => $value) {
        Console::print ($value[0]);
        $req_values[$key] = Console::get_input("Value for {$key} (default {$value[1]}): ", $value[1]);
        Console::print ();
    }

    if (file_put_contents(".env", json_encode($req_values, JSON_PRETTY_PRINT)) !== false) {
        Console::success("Written the configuration file. Now run '{$argv[0]} {$req_values['ROOT_KEY']} init' to spin up the tables and settings");
    }
    else {
        Console::error("Error in writing file. Try manually");
        Console::print ("\n" . json_encode($req_values, JSON_PRETTY_PRINT) . "\n");
    }
}
else {
    // check arguments
    if ($argc !== 3) {
        Console::fatal($usage);
    }
    if (array_search($argv[2], $available_modes) === false) {
        Console::fatal("Given mode '{$argv[2]}' is invalid\n\nAVAILABLE MODES - " . implode(", ", $available_modes) . "\n");
    }

    $mode = $argv[2];
    $root_key = $argv[1];

    // check if key provided if the root key
    if ($root_key !== $KEYS->ROOT_KEY) {
        Console::fatal("incorrect key '{$root_key}'");
    }
    
    if ($mode === "init") {
        DbInit::connect($KEYS->DATABASE_HOST, $KEYS->DATABASE_USERNAME, $KEYS->DATABASE_PASSWORD, $KEYS->DATABASE_NAME);
        DbInit::delete_tables();
        DbInit::create_tables();
        
        Console::task("User tasks");
        $user = new User();
        $user->username = "root";
        $user->set_password($KEYS->ROOT_PASSWORD);
        $user->role = "admin";
        
        if ($user->register() === false) {
            Console::fatal("Couldn't create root user");
        }
        Console::success("Created root user");
        Console::print("Users tasks complete\n");
    } 
    else if ($mode === "tables-create") {
        DbInit::connect($KEYS->DATABASE_HOST, $KEYS->DATABASE_USERNAME, $KEYS->DATABASE_PASSWORD, $KEYS->DATABASE_NAME);
        DbInit::delete_tables();
        DbInit::create_tables();
    }
    else if ($mode === "tables-delete") {
        DbInit::connect($KEYS->DATABASE_HOST, $KEYS->DATABASE_USERNAME, $KEYS->DATABASE_PASSWORD, $KEYS->DATABASE_NAME);
        DbInit::delete_tables();
    }
}

class DbInit {

    public static $db = null;

    static function connect($host, $user, $pass, $dbname) {
        self::$db = @mysqli_connect($host, $user, $pass, $dbname);
        if (!self::$db || mysqli_connect_errno()) {
            Console::fatal("Couldn't connect to database.[Error: " . mysqli_connect_error() . "]");
        }
    }

    static function delete_tables() {
        Console::task("Deleting old tables");
        $queries = array(
            "Users" => "DROP TABLE IF EXISTS Users",
            "Systems" => "DROP TABLE IF EXISTS Systems",
        );
        foreach ($queries as $key => $query) {
            if (!mysqli_query(self::$db, $query)) {
                Console::print (mysqli_error(self::$db));
                Console::fatal("Couldn't delete {$key} table");
            }
            Console::success("Deleted {$key} table");
        }
        Console::print ("Deleted all previous tables\n");
    }

    static function create_tables() {
        Console::task("Creating new tables");
        $queries = [
            "Users" => "CREATE TABLE IF NOT EXISTS Users (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                username VARCHAR(255) NOT NULL UNIQUE,
                password VARCHAR(100) NOT NULL,
                role ENUM('admin', 'user') NOT NULL,
                created BIGINT NOT NULL
            )",
            "Systems" => "CREATE TABLE IF NOT EXISTS Systems (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(255) NOT NULL UNIQUE,
                system_key VARCHAR(100) NOT NULL,
                endpoint VARCHAR(100) NOT NULL UNIQUE,
                last_update BIGINT NULL,
                visibility ENUM('all', 'restricted') DEFAULT 'all',
                created BIGINT NOT NULL
            )",

        ];

        foreach ($queries as $key => $query) {
            if (!mysqli_query(self::$db, $query)) {
                Console::print (mysqli_error(self::$db));
                Console::fatal("Couldn't create {$key} table");
            }
            Console::success("Created {$key} table");
        }
        Console::print ("Created all new tables\n");

    }

    static function close() {
        (self::$db !== null) ? mysqli_close(self::$db) : null;
    }
}

DbInit::close();
Console::end();

