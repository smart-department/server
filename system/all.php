<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/../class/autoloader.php");

session_start();
$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";

if ($method === "GET" && !empty($_SESSION["username"])) {
    $admin = $_SESSION["role"] === "admin";

    Database::init();
    if ($admin) {
        $systems = Database::query("SELECT id, name, endpoint, created, system_key, last_update, visibility FROM Systems");
    } else {
        $systems = Database::query("SELECT id, name, endpoint, created, system_key, last_update, visibility FROM Systems WHERE visibility = :visibility", [":visibility" => "all"]);
    }
    
    $ids = [];
    for ($i = 0; $i < count($systems); $i++) {
        $system = new System();
        foreach ($systems[$i] as $key => $value) {
            $system->$key = $value;
        }
        $system->get_overview();
        $systems[$i] = $system;
        if (empty($system->info->message)) {
            $ids[] = $system->id;
        }
    }

    // update last seen times
    if (count($ids) > 0) {
        Database::query("UPDATE Systems SET last_update = :last_update WHERE id IN (". implode(',', $ids) .")", [ ":last_update" => time() ]);
    }
    
    
    Response::send($systems);

} else {
    Response::not_found();
}