<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/../class/autoloader.php");

session_start();
$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";
$url_preg = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";

if ($method === "GET" && !empty($_SESSION["username"]) && !empty($_GET["id"])) {
    $id = $_GET["id"];

    $system = new System();
    $get_from_all = $_SESSION["role"] === "admin";
    if ($system->get($id, $get_from_all) === false) {
        Response::send(null, 400, "System with id {$id} does not exist");
    }
    
    if (($message = $system->shutdown()) === false) {
        Response::send(null, 400, "System '{$system->name}' not shutdown");
    }

    Response::send(null, 200, $message);
} else {
    Response::not_found();
}