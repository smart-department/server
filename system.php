<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/class/autoloader.php");

session_start();
$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";
$url_preg = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";

if ($method === "GET" && !empty($_SESSION["username"]) && !empty($_GET["id"])) {
    $id = $_GET["id"];

    $system = new System();
    $get_from_all = $_SESSION["role"] === "admin";
    if ($system->get($id, $get_from_all) === false) {
        Response::send(null, 400, "System with id {$id} does not exist");
    }
    
    Response::send($system);

} else if ($method === "POST" && !empty($_SESSION["role"]) && $_SESSION["role"] === "admin") {

    $request = Request::parse();

    $errors = [];
    if (empty($request->name)) {
        array_push($errors, "name is empty");
    }
    if (empty($request->system_key)) {
        array_push($errors, "system_key is empty");
    } else if (strlen($request->system_key) < 1 || strlen($request->system_key) > 100) {
        array_push($errors, "system_key should be between 1 and 100 characters");
    }
    if (empty($request->endpoint)) {
        array_push($errors, "endpoint is empty");
    } else if (!preg_match($url_preg, $request->endpoint)) {
        array_push($errors, "invalid endpoint");
    }
    if (!empty($errors)) {
        Response::send(null, 404, implode("\n", $errors));
    }
    
    $system = new System();
    $system->name = $request->name;
    $system->system_key = $request->system_key;
    $system->endpoint = $request->endpoint;
    $system->visibility = $request->visibility;
    
    if ($system->create() === false) {
        Response::send(null, 400, "Could not create system (invalid visibility ?)");
    }
    
    Response::send($system, 201, "New system created");
    
} else if ($method === "PATCH" && !empty($_SESSION["role"]) && $_SESSION["role"] === "admin" && $_GET["id"]) {
    $id = $_GET["id"];
    $request = Request::parse();
    
    $system = new System();
    if ($system->get($id, true, true) === false) {
        Response::send(null, 404, "System with id {$id} does not exist");
    }
    
    $updatable_values = ["name", "system_key", "endpoint", "visibility"];
    foreach ($updatable_values as $val) {
        $system->$val = empty($request->$val)? $system->$val: $request->$val;
    }
    
    $errors = [];
    if (!empty($request->system_key) && (strlen($request->system_key) < 1 || strlen($request->system_key) > 100)) {
        array_push($errors, "system_key should be between 1 and 100 characters");
    }
    if (!empty($request->endpoint) && !preg_match($url_preg, $request->endpoint)) {
        array_push($errors, "invalid endpoint");
    }

    if (!empty($errors)) {
        Response::send(null, 404, implode("\n", $errors));
    }
    
    if ($system->update() === true) {
        Response::send($system, 200, "Updated system {$system->name}");
    }

    Response::send(null, 500, "Error in updating system");

} else if ($method === "DELETE" && !empty($_SESSION["role"]) && $_SESSION["role"] === "admin" && $_GET["id"]) {
    $id = $_GET["id"];

    $system = new System();
    if ($system->get($id, true, true) === false) {
        Response::send(null, 400, "System with {$id} does not exist");
    }

    if ($system->delete() === true) {
        Response::send(null, 200, "Deleted system {$system->name}");
    }
    Response::send(null, 500, "Error in deleting system");
} else {
    Response::not_found();
}