<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ ."/../class/autoloader.php");

session_start();
$method = (!empty($_SERVER["REQUEST_METHOD"]))? $_SERVER["REQUEST_METHOD"]: "GET";

if ($method === "GET" && !empty($_SESSION["username"]) && $_SESSION["role"] === "admin") {
    $admin = $_SESSION["role"] === "admin";
    
    Database::init();
    $users = Database::query("SELECT id, username, role, created FROM Users WHERE username != :username OR username = :current", [":username" => "root", ":current" => $_SESSION["username"]]);
    
    for ($i = 0; $i < count($users); $i++) {
        $user = new User();
        foreach ($users[$i] as $key => $value) {
            $user->$key = $value;
        }
        $users[$i] = $user;
    }
    
    Response::send($users);

} else {
    Response::not_found();
}